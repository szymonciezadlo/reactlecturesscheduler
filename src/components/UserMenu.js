import API from "../services/api";
import {Button, Form, Input, Layout, Menu, notification} from "antd";
import {ClockCircleOutlined, LockOutlined, LoginOutlined, MailOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import {useHistory} from 'react-router-dom'


export const UserMenu = () => {
    const history = useHistory();
    const {Sider} = Layout;
    const {SubMenu} = Menu;
    const [collapsed, setCollapsed] = useState(true);
    const [form] = Form.useForm();
    const [userMenu, setUserMenu] = useState(false); // is logged in or not

    const handleReminderClick = () => {
        history.push('/reminders')
    };

    useEffect(() => {
        if (localStorage.getItem('token')) {
            setUserMenu(true)
        }
    }, []);

    const onCollapse = () => {
        setCollapsed(!collapsed);
    };


    function handleOnSignIn() {
        history.push('signIn');

    }

    const handleOnLogin = values => {

        API.post('/auth', {}, {
            auth: {
                username: values.email,
                password: values.password
            }
        })
            .then(response => {
                localStorage.setItem("token", response.data.token);
                setUserMenu(true);
            })
            .catch(() => {
                notification['error']({
                    message: 'Błąd logowania!',
                    description: 'Podałeś nieprawidłowe dane w formularzu'
                })
            })
    };



    const loginMenu =
        <SubMenu icon={<LoginOutlined/>} title={"Log In"}>
            <>
                <Form name="loginForm" form={form} onFinish={handleOnLogin}>
                    <Form.Item name="email"
                               rules={[{required: true, message: 'Please input your Email!'}]}>
                        <Input prefix={<MailOutlined/>}
                               placeholder='Email'/>
                    </Form.Item>
                    <Form.Item name="password"
                               rules={[{required: true, message: 'Please input your Password!'}]}>
                        <Input prefix={<LockOutlined/>}
                               type='password' placeholder='Password'/>
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit' key={'buttonLogIn'}>
                            Login
                        </Button>
                        <Button htmlType="button" onClick={handleOnSignIn}>
                            SignIn!
                        </Button>
                    </Form.Item>
                </Form>
            </>
        </SubMenu>;

    const reminderItem = <Menu.Item icon={<ClockCircleOutlined/>} key={"reminder"}
                                    onClick={handleReminderClick}>Reminders</Menu.Item>;

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} reverseArrow={true}>
            <Menu theme="dark" defaultSelectedKeys={"userSider"} mode="inline">
                {userMenu === false ? loginMenu : reminderItem}
            </Menu>
        </Sider>
    )
};
