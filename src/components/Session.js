import React from 'react';
import {Table} from 'antd'
import 'antd/dist/antd.css';


export const Session = ({sessionName, listOfPresentations}) => {

    const columns =
        [
            {
                title: 'Time',
                dataIndex: 'time',
                sorter: (a, b) => a.sortTime - b.sortTime,
                sortOrder: 'ascend'

            },
            {
                title: 'Title',
                dataIndex: 'title',

                render: (text, record) => record.ref ? (
                    <a href={"https://ie2020.kisim.eu.org/api/abstracts/" + record.ref}>{text}</a>) : <p>{text}</p>
            }
        ];


    const data = [];
    for (const pres in listOfPresentations) {
        const timeOfPresInHours = new Date(listOfPresentations[pres].date).getHours();
        const timeOfPresInMinutes =
            new Date(listOfPresentations[pres].date).getMinutes() === 0 ?
                '00' : new Date(listOfPresentations[pres].date).getMinutes();
        data.push({
            key: listOfPresentations[pres].title + timeOfPresInHours + timeOfPresInMinutes,
            ref: listOfPresentations[pres].filename,
            time: timeOfPresInHours + ":" + timeOfPresInMinutes,
            title: listOfPresentations[pres].title,
            sortTime: new Date(listOfPresentations[pres].date).getTime(),
        })
    }


    return (
        <>
            <Table pagination={false} columns={columns} dataSource={data} bordered
                   title={() => <h1>{sessionName}</h1>}/>
        </>);


};
