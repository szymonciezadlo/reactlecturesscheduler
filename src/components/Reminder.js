import React, {useEffect, useState} from 'react';
import API from "../services/api";
import {Card, Col, Row, notification, Form, Button, Select, Switch, Layout} from 'antd';
import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined";
import SmileOutlined from "@ant-design/icons/lib/icons/SmileOutlined";
import {useHistory} from "react-router-dom";

const {Meta} = Card;
const {Option} = Select;
const {Footer, Content} = Layout;

export const Reminder = () => {
    const [form] = Form.useForm();
    const [options, setOptions] = useState([]);
    const [reminders, setReminders] = useState([]);
    const [presentationsList, setPresentationsList] = useState([]);
    const header = API.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');

    const history = useHistory();

    useEffect(() => {
        if (header.split(" ")[1] === 'null') {
            history.push('/');
        } else {
            async function getData() {
                const promiseReminders = API.get('/reminders', {}, header);
                const promisePresentations = API.get('/presentations');

                await Promise.all([promiseReminders, promisePresentations])
                    .then(function (values) {
                        /*                console.log(values[0].data);
                                        console.log(values[1].data);*/
                        const presentations = values[1].data;
                        setPresentationsList(presentations);
                        const optionList =presentations.map(onePres => {
                            return (<Option key={"option" + onePres.id} value={onePres.id}>{onePres.title}</Option>)
                        });
                        setOptions(optionList);
                        const grid = values[0].data.map(oneRem => {
                            const result = findPres(presentations, oneRem);
                            return (
                                {
                                    rem: oneRem,
                                    title: result.title,
                                    date: result.date
                                });
                        });
                        setReminders(grid);
                    })
                    .catch(() => {
                        localStorage.clear();
                        history.push('/');
                    })
            }
            getData();
        }
    }, [header,history]);

    function findPres(presentations, oneRem) {
        return presentations.filter(function (chain) {
            return chain.id === oneRem.presentationId;
        })[0];
    }



    function makeCardGrid(rem, resPresTitle, resPresDate) {
        const gridStyle = {
            //width: '20%',
            textAlign: 'center',
            backgroundColor: '#DCDCDC',
            fontSize: 16,
            fontWeight: 'bold',
            color: '#808080',
        };

        const timePres = new Date(resPresDate);
        const timePresMin = timePres.getMinutes() === 0 ? '00' : timePres.getMinutes();
        if (rem.enabled === false) {
            gridStyle.backgroundColor = '#793F4E';
            gridStyle.color='#FFFAF0';
        }

        return (<Col xs={24} sm={12} md={8} lg={6} flex key={"col" + rem.id}>
            <Card
                actions={[
                    <DeleteOutlined
                        key={"delete" + rem.id}
                        onClick={() => deleteRem(rem.id)}/>,     //tutaj przypisuje funkcje do przycisku, dzieje sie to PRZED zapisaniem stanu reminders
                    <Switch
                        key={"put" + rem.id}
                        defaultChecked={rem.enabled}
                        onChange={(e) => putRem(rem.id, rem.presentationId, e)}/>]}
                key={rem.id}
                style={gridStyle}>
                <Meta style={{height: 120}} description={<p style={{color: gridStyle.color}}>{
                    resPresTitle}<br/>{
                    resPresDate.split('T')[0] + '  ' + timePres.getHours() + ':' + timePresMin}</p>}/>
            </Card>
        </Col>);
    }

    function putRem(id, presentationId, checked) {
        //console.log(checked);
        //console.log(id)
        API.put(
            'reminders/' + id,
            {
                presentationId: presentationId,
                notes: "string",
                enabled: checked
            },header)
            .then(function () {
                    const newArray = reminders.filter(function (chain) {
                        if (chain.rem.id === id) {
                            chain.rem.enabled = checked;
                        }
                        return chain;
                    });
                    setReminders(newArray);
                    notification.open({
                        message: 'Reminder zmieniony',
                        description:
                            'Changed success',
                        icon: <SmileOutlined style={{color: '#108ee9'}}/>
                    });
                    //getData();
                }
            )
            .catch(() => {
                notification['error']({
                    message: 'Błąd zmiany remindera!',
                    description: "Coś poszło nie tak"
                })
            })

    }

    function deleteRem(e) {
        API.delete('reminders/' + e)
            .then(function () {
                    /*                    console.log(reminders)   //tutaj dostaje pusta tablice pomimo ze stan jest z pewnoscia nie pusty w momencie klikniecia, poniewaz inaczej nie bylo by ani karty ani przycisku
                                        console.log(presentationsList);*/
                    const newRems = reminders.filter(function (chain) {
                        return chain.rem.id !== e;
                    });
                    setReminders(newRems);
                    //getData();
                    notification.open({
                        message: 'Reminder deleted',
                        description:
                            'Deleted success',
                        icon: <SmileOutlined style={{color: '#108ee9'}}/>
                    });
                }
            )
            .catch(() => {
                notification['error']({
                    message: 'Błąd usuwania remindera!',
                    description: "Coś poszło nie tak"
                })
            })

    }

    const handleAddReminder = e => {
        API.post('/reminders', {"presentationId": e.presentation}, header)
            .then(
                function (values) {
                    const rem = values.data;
                    const onePres = findPres(presentationsList, rem);
                    /*                    console.log(reminders);                                      // tutaj reminders jest niepuste
                                        console.log(presentationsList);*/
                    //setReminders([...reminders, makeCardGrid(rem, onePres.title, onePres.date)]);
                    setReminders(
                        [...reminders,
                            {
                                rem: rem,
                                title: onePres.title,
                                date: onePres.date
                            }
                        ]);

                    notification.open({
                        message: 'Reminder added',
                        description:
                            'Everything is fine.',
                        icon: <SmileOutlined style={{color: '#108ee9'}}/>
                    });
                }
            )
            .catch(() => {
                notification['error']({
                    message: 'Błąd dodawania remindera!',
                    description: 'Podałeś nieprawidłowe dane w formularzu'
                })
            })
    };

    return (
        <>
            <Layout>
                <Content>
                    <Card key={"reminders"} title="Reminders">
                        <Row gutter={[16,16]} key={"remindersRow"} type="flex" justify="left" align="top">
                            {reminders.map(rem => makeCardGrid(rem.rem, rem.title, rem.date))}
                        </Row>
                    </Card>
                </Content>
            </Layout>


            <Footer>
                <Form name="addReminderForm" layout={"inline"} form={form} onFinish={handleAddReminder}>
                    <Form.Item name="presentation" placeholder="Select presentation">
                        <Select style={{width: 400}}>{options}</Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit' key={'buttonAddReminder'}>Add</Button>
                    </Form.Item>
                </Form>
            </Footer>
        </>
    );
};
