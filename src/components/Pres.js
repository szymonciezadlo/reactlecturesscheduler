//Wersja pierwsza zwraca <li> tylko danego dnia



export const Presentation = (sessionName, startSession,endSession, backendPresentations) => {
    const startSessionInDays = new Date(startSession).getDate();
    const startSessionInHours = new Date(startSession).getHours();
    const endSessionInHours = new Date(endSession).getHours();

    if (Object.keys(backendPresentations).length) {
        return backendPresentations.filter(function (pres) {
                const presDateHours = new Date(pres.date).getHours();
                const presDateDay = new Date(pres.date).getDate();
                return presDateHours >= startSessionInHours &&
                    presDateHours <= endSessionInHours &&
                    pres.session === sessionName &&
                    presDateDay===startSessionInDays;
            }
        );
    }
};

