import React from "react";
import { Layout, Menu} from 'antd'
import 'antd/dist/antd.css';
import {useHistory} from "react-router-dom";


const {Header} = Layout;

export const Navigation = ({day,setDay}) => {
    const history = useHistory();
    const handleMenuClick = e => {
        setDay(e.key);
        history.push('/');
    };


    return (
        <>
            <Layout key={'page'}>
                <Header>
                    <Menu mode={"horizontal"} onClick={handleMenuClick} theme={"dark"}>
                        <Menu.Item key={"MONDAY"}>Monday</Menu.Item>
                        <Menu.Item key={"TUESDAY"}>Tuesday</Menu.Item>
                        <Menu.Item key={"WEDNESDAY"}>Wednesday</Menu.Item>
                        <Menu.Item key={"THURSDAY"}>Thursday</Menu.Item>
                        <Menu.Item key={"FRIDAY"}>Friday</Menu.Item>
                    </Menu>
                </Header>

            </Layout>

        </>
    )

};

