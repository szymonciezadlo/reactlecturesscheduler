import React, {useEffect, useState} from 'react';
import API from "../services/api";
import {Session} from "./Session";
import {Alert, Col, Row, Skeleton} from "antd";
import {Presentation} from "./Pres";


export const Schedule = ({day}) => {

    const [gridView, setGridView] = useState(<Skeleton active/>);

    function makeGrid(backendSchedules, backendSessions, backendPresentations) {
        return backendSchedules.map(temp => {
            const row = temp.sessions.map(singleSession => {
                const listPresentations = Presentation(singleSession, temp.start, temp.end, backendPresentations);
                if (Object.keys(listPresentations).length) {
                    const lengthCol = 24 / Object.keys(temp.sessions).length;
                    return (<Col
                        xs={24} sm={24} md={lengthCol * 1.5} lg={lengthCol}
                        key={singleSession + temp.end}>
                        <Session key={singleSession + temp.start} sessionName={backendSessions[singleSession].name}
                                 listOfPresentations={listPresentations}/>
                    </Col>);
                }
                return 0;
            });
            return (
                <Row gutter={[{xs: 2, sm: 2, md: 8, lg: 16}, {xs: 2, sm: 2, md: 8, lg: 16}]} key={"row" + temp.start}
                     type="flex" justify="space-around" align="top">{row}</Row>);
        });

    }


    useEffect(() => {
        async function fetchMyAPI() {

            const promiseSchedules = API.get('/schedules?day=' + day);
            const promiseSessions = API.get('/sessions');
            const promisePresentations = API.get('/presentations');

            await Promise.all([promiseSchedules, promiseSessions, promisePresentations])
                .then(function (values) {
                    const newGrid = makeGrid(values[0].data[0][day], values[1].data, values[2].data);
                    setGridView(newGrid);
                }).catch(err => {
                    setGridView(<Alert showIcon type={"error"} message={"Error"}
                                       description={'' + err}/>)
                })
        }

        fetchMyAPI();
    }, [day]);

    return gridView;
};

