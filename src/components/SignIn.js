import {Button, Form, Input} from "antd";
import React from "react";
import {LockOutlined, MailOutlined} from "@ant-design/icons";
import API from "../services/api";
import {useHistory} from "react-router-dom";

export const SingIn = () => {

    const [form] = Form.useForm();
    const history = useHistory();

    function handleOnSignIn(values) {
        API.post(
            '/users',
            {
                email: values.email,
                password: values.password
            }
        )
            .then((response)=>{
                //console.log(response)
                localStorage.setItem("token",response.data.token);
                history.push('/');

            })
    }


    return (
        <>
            <br/>
            <Form name="signInForm" form={form} onFinish={handleOnSignIn}>
                <Form.Item name="email"
                           rules={[{required: true, message: 'Please input your Email!'}]}>
                    <Input prefix={<MailOutlined/>} style={{width: 400}}
                           placeholder='Email'/>
                </Form.Item>
                <Form.Item name="password"
                           rules={[{required: true, message: 'Please input your Password!'}]}>
                    <Input prefix={<LockOutlined/>} style={{width: 400}}
                           type='password' placeholder='Password'/>
                </Form.Item>
                <Form.Item>
                    <Button htmlType="submit">
                        SignIn!
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}
