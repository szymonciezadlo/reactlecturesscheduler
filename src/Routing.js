import React, {useState} from 'react'
import {Switch, Route} from "react-router-dom";
import {Navigation} from "./components/Navigation";
import {Layout} from "antd";
import {Schedule} from "./components/Schedule";
import {UserMenu} from "./components/UserMenu";
import {Reminder} from "./components/Reminder";
import {SingIn} from "./components/SignIn";


export const Routing = () => {
    const [day, setDay] = useState("MONDAY");
    const {Content, Header} = Layout;


    return (
        <>
            <Layout>
                <Header>
                    <Navigation day={day} setDay={setDay}/>
                </Header>
                <Switch>
                    <Route exact path="/">
                        <Layout>
                            <Content>
                                <Schedule key={"schedule"} day={day}/>;
                            </Content>
                            <UserMenu/>
                        </Layout>
                    </Route>
                    <Route exact path={'/reminders'}>
                        <Reminder/>
                    </Route>
                    <Route exact path={'/signIn'}>
                        <SingIn/>
                    </Route>
                </Switch>
            </Layout>
        </>
    )
};
